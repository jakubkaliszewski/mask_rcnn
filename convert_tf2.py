import tensorflow as tf

PATH_TO_SAVE_FROZEN_PB ="."
FROZEN_NAME ="saved_model.pb"

def keras_to_tflite2(in_weight_file, out_weight_file):
    # https://github.com/matterport/Mask_RCNN/issues/2020#issuecomment-596449757
    input_arrays = ["input_image"]
    output_arrays = ["mrcnn_class/Softmax","mrcnn_bbox/Reshape"]
    
    converter = tf.lite.TFLiteConverter.from_saved_model(
        ".",
        input_arrays, output_arrays
        )

    converter.target_spec.supported_ops = [
        tf.lite.OpsSet.TFLITE_BUILTINS, # enable TensorFlow Lite ops.
        tf.lite.OpsSet.SELECT_TF_OPS # enable TensorFlow ops.
    ]

    tflite_model = converter.convert()
    open(out_weight_file, "wb").write(tflite_model)
    print("*"*80)
    print("Finished converting keras model to Frozen tflite")
    print('PATH: ', out_weight_file)    
    print("*" * 80)

keras_to_tflite2("./mask_rcnn_plate.h5","./mask_rcnn_plate.tflite")