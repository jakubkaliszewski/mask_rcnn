#/bin/bash

SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`

sudo apt update
sudo apt upgrade -y
sudo apt install python-pip python3-pip -y
sudo pip3 install tensorflow==1.13.1
sudo pip3 install Keras==2.2.4
sudo pip3 install imgaug
sudo pip3 install pycocotools

sudo pip3 install -r ${SCRIPTPATH}/requirements.txt

sudo python ${SCRIPTPATH}/setup.py install

cp ${SCRIPTPATH}/maskrcnn.service /usr/lib/systemd/system